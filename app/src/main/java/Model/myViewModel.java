package Model;

import android.os.Handler;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import api.ApiHelper;


public class myViewModel extends ViewModel {

    public interface CallbackView {
        void onSuccess(List<State> list);
        void onfailure(String string);
    }

    private CallbackView mCallback;
    //private MutableLiveData<List<State>> states;
    private List<State> states;
    ApiHelper api = new ApiHelper();
    //public LiveData<List<State>> getStates() {
    public List<State> getStates(CallbackView callback) {
        mCallback = callback;
        if (states == null) {
            //states = new MutableLiveData<List<State>>();
            states = new ArrayList<State>();

            loadStates();
        }
        return states;
    }

    private void loadStates() {
        api.start(new ApiHelper.CallbackApi() {
            @Override
            public void onSuccess(List<State> list) {
                states = list;
                mCallback.onSuccess(states);
            }

            @Override
            public void onFailure(String string) {
                mCallback.onfailure(string);
            }
        });

        //states.setValue(api.getList());
    }
}
