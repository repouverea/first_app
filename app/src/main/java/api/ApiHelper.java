package api;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import Model.State;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHelper implements Callback<List<State>> {
    public interface CallbackApi{
        void onSuccess(List<State> list);
        void onFailure(String string);
    }
    static final String BASE_URL = "https://restcountries.eu/rest/v2/";
    private List<State> list;
    private CallbackApi mCallback;
    public void start(CallbackApi myInterface) {
        mCallback = myInterface;
        list = new ArrayList<State>();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        api api = retrofit.create(api.class);

        Call<List<State>> call = api.getCities();
        call.enqueue(this);
    }

    public void onResponse (Call<List<State>> call, Response<List<State>> response){
        if (response.isSuccessful()) {
            List < State > states = response.body();
            states.forEach(state ->{

                list.add(state);
                Log.w("RESPONSE:CITY", "[state: " + state.name +", capital: " + state.capital + "]");
            });
            mCallback.onSuccess(states);

        }else {
            Log.e("RESPONSE",response.errorBody().toString());
            mCallback.onFailure("FAIL");
        }
    }

    public void onFailure (Call < List < State >> call, Throwable t){
        Log.e("REQUEST", "request failed" + t.toString());
        mCallback.onFailure("FAIL");
    }

    public List<State> getList() {
        return list;
    }
}
