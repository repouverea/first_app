package api;

import java.util.List;

import Model.State;
import retrofit2.Call;
import retrofit2.http.GET;

public interface api {
    @GET("all")
    Call<List<State>> getCities();
}
