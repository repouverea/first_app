package com.example.first_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import Model.MyAdapter;
import Model.State;
import Model.myViewModel;

public class MainActivity extends AppCompatActivity {
    RecyclerView recView;
    ProgressBar progressBar;
    MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.progress_circular);
        myViewModel model = ViewModelProviders.of(this).get(myViewModel.class);

        model.getStates(new myViewModel.CallbackView() {
            @Override
            public void onSuccess(List<State> list) {
                progressBar.setVisibility(View.INVISIBLE);
                setupRec(list);
            }

            @Override
            public void onfailure(String string) {
                Log.e("FAIL", "onfailure: FAIL");
            }
        });

    }

    public Intent getIntent(Activity from, State state ) {
        Intent intent = new Intent(from, AboutActivity.class);

        intent.putExtra("name", state.name);
        intent.putExtra("capital", state.capital);
        intent.putExtra("alpha", state.alpha3Code);
        intent.putExtra("region", state.region);
        intent.putExtra("flag", state.flag);
        return intent;
    }

    public void setupRec(List<State> list) {
        recView = findViewById(R.id.rvAnimals);
        recView.setHasFixedSize(true);
        recView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyAdapter(this , list);
        adapter.setClickListener(new MyAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                startActivity(getIntent(MainActivity.this, list.get(position)));
            }
        });
        recView.setAdapter(adapter);
    }


}
